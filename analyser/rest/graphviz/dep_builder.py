#! /usr/bin/env python
#coding=utf-8

class DepBuilder(object):
	def _getStyle(self, dep):
		if dep["chipsetsdk"]:
			return 'color=blue'
		if dep["platformsdk"]:
			return 'color=red'
		if not dep["external"]:
			return 'style=dashed, color=grey, arrowhead="vee"'
		return ''

	def getGraphVizInfo(self, context):
		dep = context.obj
		linkStr = "javascript:top.fnEvtShowSymbolsForDeps(%d)" % dep["id"]
		labelStr = "<<u>%s</u>>" % str(dep["calls"]) # underline
		return "m%d -> m%d [label=%s, %s href=\"%s\", fontcolor=blue];\n" % (dep["caller"]["id"], dep["callee"]["id"], labelStr, self._getStyle(dep), linkStr)
