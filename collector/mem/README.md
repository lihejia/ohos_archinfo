# OpenHarmony内存数据采集模块

本模块用于对内存采集的smaps相关文件进行分析，提取每个进程的内存以及线程信息。



## 1. smaps.py 单个smaps文件解析模块

内存的Virtual Memory Area示例如下：

```
00841000-00852000 r--p 00000000 b3:06 226                                /system/bin/init
Size:                 68 kB
KernelPageSize:        4 kB
MMUPageSize:           4 kB
Rss:                  68 kB
Pss:                  66 kB
Shared_Clean:          4 kB
Shared_Dirty:          0 kB
Private_Clean:        64 kB
Private_Dirty:         0 kB
Referenced:           68 kB
Anonymous:             0 kB
LazyFree:              0 kB
AnonHugePages:         0 kB
ShmemPmdMapped:        0 kB
FilePmdMapped:         0 kB
Shared_Hugetlb:        0 kB
Private_Hugetlb:       0 kB
Swap:                  0 kB
SwapPss:               0 kB
Locked:                0 kB
THPeligible:    0
VmFlags: rd mr mw me dw
```

每个进程的smaps文件由多个上述VMA组成，每个VMA归属于一个对象，对象包括文件和heap等。

本文件中VirtualMemroyArea

进程的内存[smaps](https://www.jianshu.com/p/8203457a11cc)信息主要包括以下字段：

"Size", "Rss", "Pss", "Swap", "SwapPss", "Shared_Clean", "Shared_Dirty", "Private_Clean", "Private_Dirty", "Referenced", "Anonymous", "KernelPageSize", "MMUPageSize", "LazyFree", "AnonHugePages", "ShmemPmdMapped", "Shared_Hugetlb", "Private_Hugetlb", "Locked", "THPeligible", "FilePmdMapped"

smaps.py中定义了ProcessSmaps，SmapsObject以及VirtualMemroyArea三个类，这三个类都含有以上字段信息，详细内容如下：

## 1.1 ProcessSmaps

ProcessSmaps用于解析管理单个进程的smaps文件内存信息，对应/proc/{pid}/smaps文件，以下是两个核心的成员：

| 字段名称 | 说明                                               |
| -------- | -------------------------------------------------- |
| objects  | 进程中smaps对象个数总和。                          |
| vmas     | 进程中每个smaps包含的Virtual Memory Area个数总和。 |

ProcessSmaps中汇总了该进程每个对象的smaps信息。

## 1.2 SmapsObject



## 1.3 VirtualMemroyArea



1.2