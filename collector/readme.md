# OpenHarmony实时架构信息collector模块说明

collector模块根据编译产物，收集各个模块的架构信息，形成结构化数据，便于[analyser](../analyser/README.md)进行分析与展示。其总体架构如下：

![image-20221107213407810](img/collector.png)



## 1. 使用方法

### 1.1 准备工作

如上图所示，collector需要指定输入产物，有两种类型的输入产物：

#### 1.1.1 全量源码编译产物

全量源码编译完成后，通过[collector.py](collector.py)文件的--input选项指定输入产物目录，一般为out/rk3568等产品的编译输出根目录。该目录下的关键内容为：

```sh
out/rk3568
  packages/phone/images/system.img
    system：存放system.img镜像中的system内容；
    vendor：存放vendor.img镜像的的内容
  obj/drivers/hdf_core/framework/tools/hc-gen/hc-gen 	# hdi文件解析工具
  build_configs/parts_info/inner_kits_info.json   	    # innerapi信息
  packages/phone/system_module_info.json				# 各模块的部件归属信息
```

#### 1.1.2 最终镜像产物

对于独立系统组件和芯片组件工程编译的产物，可以直接分析system.img和vendor.img，输入文件包括：

```sh
system组件归档文件：
	 system.img
     inner_kits_info.json
     system_module_info.json

vendor组件归档文件：
     vendor.img
     inner_kits_info.json
     system_module_info.json
```

服务器工具要求：

> python
>
> mklibs：sudo apt install mklibs



### 1.2 收集命令

通过python2 collector.py --input path [--output db_path] [-s --scan] [-m --mem]命令开始收集，收集完成后会生成archinfo.db数据库文件。各选项的说明如下：

| 选项     | 参数             | 必选 | 说明                                                         |
| -------- | ---------------- | ---- | ------------------------------------------------------------ |
| --input  | input asset path | 是   | 输入的静态文件路径                                           |
| --output | output db path   | 否   | 输出的db数据库文件路径，如果没有指定，则字节生成到--input指定的目录 |
| --mem    | 无               | 否   | 仅更新内存信息                                               |
| --scan   | 无               | 否   | 自动扫描--input下两级目录各个产品版本的输入文件              |
| --extract     | 无             | 否   | 表示输入img镜像文件，需要进行解压分析                    |

### 1.3 增量收集说明

collector全量收集过程比较耗时，可以注释collector.py中SymbolDBBuilder._add_all函数中的步骤来增量收集，减少收集时间。



## 2. 数据库说明

### 2.1 ELF文件相关表说明

| 表名称       | 说明                                        |
| ------------ | ------------------------------------------- |
| symbols      | 各个ELF文件提供的符号列表                   |
| undefines    | 各个ELF文件未定义符号表                     |
| calls        | 任意两个存在依赖关系的ELF文件之间的调用符号 |
| dependencies | 每个ELF文件的直接依赖关系列表               |
| indirects    | 每个ELF文件的间接依赖关系列表               |
| modules      | 每个ELF文件的详细信息                       |

视图：

| 表名称           | 说明                                                         |
| ---------------- | ------------------------------------------------------------ |
| depends          | dependencies展开caller和callee以及对应的component信息的视图  |
| indirect_details | indirects间接依赖表展开caller和callee以及对应的component信息的视图 |
| call_details     | calls调用表展开符号信息以及caller、callee详细信息的视图      |

#### 2.1.1 symbols表字段说明

| 字段      | 类型   | 说明                              |
| --------- | ------ | --------------------------------- |
| id        | int    | ElfFile["id"] << 16 + 自身的idx   |
| parent_id | int    | 符号所属ELF文件的ID               |
| name      | string | 符号名称                          |
| version   | string | 符号版本                          |
| library   | string | 符号所属ELF文件名称               |
| weak      | int    | 是否是weak符号                    |
| demangle  | string | 通过c++filt命令decode展开的符号名 |

#### 2.1.2 undefines表字段说明

| 字段      | 类型   | 说明                              |
| --------- | ------ | --------------------------------- |
| parent_id | int    | 符号所属ELF文件的ID               |
| name      | string | 符号名称                          |
| version   | string | 符号版本                          |
| library   | string | 符号所属ELF文件名称               |
| weak      | int    | 是否是weak符号                    |
| demangle  | string | 通过c++filt命令decode展开的符号名 |

#### 2.1.3 calls表字段说明

| 字段          | 类型 | 说明           |
| ------------- | ---- | -------------- |
| caller_id     | int  | 调用模块的ID   |
| callee_id     | int  | 被调用模块的ID |
| symbol_id     | int  | 符号ID         |
| dependence_id | int  | 依赖ID         |

#### 2.1.4 dependencies表字段说明

| 字段        | 类型 | 说明                                                 |
| ----------- | ---- | ---------------------------------------------------- |
| id          | int  | 依赖ID                                               |
| caller_id   | int  | 调用模块的ID                                         |
| callee_id   | int  | 被调用模块的ID                                       |
| calls       | int  | 调用符号个数                                         |
| external    | int  | 是否是跨部件依赖                                     |
| platformsdk | int  | 是否是应用加载的NAPI对系统组件Platform SDK模块的依赖 |
| chipsetsdk  | int  | 是否是芯片组件对系统组件Chipset SDK模块的依赖        |

#### 2.1.5 indirects表字段说明

| 字段      | 类型 | 说明             |
| --------- | ---- | ---------------- |
| id        | int  | 依赖ID           |
| caller_id | int  | 调用模块的ID     |
| callee_id | int  | 被调用模块的ID   |
| external  | int  | 是否是跨部件依赖 |

#### 2.1.6 modules表字段说明



### 2.2 内存信息相关表说明

| 表名称    | 说明               |
| --------- | ------------------ |
| processes | 进程列表           |
| objects   | 进程加载的对象列表 |
| smaps     | smaps信息          |
| vmas      | 每个smaps的vma信息 |



## 3. 代码说明

## 3.1 elf_walker.py ELFWalker

指定product_out_path（对应[collector.py](collectory.py)文件的--input输入目录），自动搜索该目录下packages/phone/system和packages/phone/vendor中的ELF文件（bin和so）。

get_elf_files返回所有的ELF文件列表（不包含软连接）。

get_link_file_map返回软链接映射表（dict）。



## 3.2 symbols.py

定义Symbol, UndefinedSymbol以及ProvidedSymbol：

| 字段      | 类型   | 说明                                    |
| --------- | ------ | --------------------------------------- |
| id        | int    | ElfFile["id"] << 16 + 自身的idx         |
| parent_id | int    | 在添加ElfFile时设置                     |
| name      | Symbol | 符号名称                                |
| version   | Symbol | 符号版本                                |
| library   | Symbol |                                         |
| weak      | Symbol |                                         |
| demangle  | Symbol | ProvidedSymbol中才通过c++filt命令decode |



## 3.3 elf_file.py ElfFile

dict字段说明：

| 字段      | 类型 | 说明 |
| --------- | ---- | ---- |
| name      | str  |      |
| type      | str  |      |
| path      | str  |      |
| size      | int  |      |
| text_size | int  |      |
| data_size | int  |      |
| bss_size  | int  |      |

方法说明：

| 字段              | 返回值类型        | 说明                                                    |
| ----------------- | ----------------- | ------------------------------------------------------- |
| is_library        | bool              | 返回是否是Library                                       |
| get_file          | str               | ELF文件路径                                             |
| provided_symbols  | [ProvidedSymbol]  | mklibs-readelf --print-symbols-provided返回的公开符号   |
| undefined_symbols | [UndefinedSymbol] | mklibs-readelf --print-symbols-undefine返回的未定义符号 |
| library_depends   | [str]             | mklibs-readelf --print-needed返回的依赖库名称           |

## 3.4 elf_file_mgr.py



### 3.4.1 ElfFileWithDepsInfo继承自ElfFile

dict字段说明：

| 字段                | 类型         | 说明 |
| ------------------- | ------------ | ---- |
| deps                | [Dependency] |      |
| deps_indirect       | [ElfFile]    |      |
| deps_total          | int          |      |
| depth               | int          |      |
| dependedBy          | [Dependency] |      |
| dependedBy_indirect | [ElfFile]    |      |
| dependedBy_total    | int          |      |
| dependedBy_depth    | int          |      |
| path                | str          |      |
| size                | int          |      |
| text_size           | int          |      |
| data_size           | int          |      |
| bss_size            | int          |      |
|                     |              |      |

方法说明：

| 字段               | 返回值类型       | 说明                   |
| ------------------ | ---------------- | ---------------------- |
| provided_symbols   | [ProvidedSymbol] | 会缓存符号列表         |
| get_symbol_by_name | ProvidedSymbol   | 从缓存中获取匹配的符号 |
| matchCalls         | int              | 与依赖的库进行符号匹配 |



### 3.4.2 Dependency

dict字段说明：

| 字段      | 类型    | 说明 |
| --------- | ------- | ---- |
| id        | int     |      |
| caller_id | int     |      |
| callee_id | Int     |      |
| caller    | ElfFile |      |
| callee    | ElfFile |      |
| calls     | int     |      |

方法说明：

| 字段            | 返回值类型 | 说明         |
| --------------- | ---------- | ------------ |
| getMatchedCalls | int        | 匹配的符号数 |

### 3.4.3 ElfFileMgr

方法说明：

| 字段            | 返回值类型 | 说明 |
| --------------- | ---------- | ---- |
| add_elf_file    |            |      |
| add_dependence  |            |      |
| get_elf_by_path |            |      |
| get_elf_by_idx  |            |      |
| get_elf_by_name |            |      |
| get_all         |            |      |

扫描ELF文件时，可在_scan_all_elf_files中忽略不需要分析的文件。
